var gulp        = require( "gulp" );
var sass        = require( "gulp-sass" );
var compass     = require( "gulp-compass" );
var minifycss   = require( "gulp-minify-css");
var uglify      = require( "gulp-uglify");
var coffee      = require( "gulp-coffee" );
var gutil       = require( "gulp-util" );
var jade        = require( "gulp-jade" );
var rename      = require( "gulp-rename" );
var concat      = require( "gulp-concat" );
var browserSync = require( "browser-sync" );
var clean       = require( "gulp-clean" );
var browserify  = require( "gulp-browserify" );
var html        = require( "html-browserify" );
var bpj         = require( "browserify-plain-jade" );
var runSequence = require( "run-sequence" );
var i18n        = require( 'gulp-html-i18n' );
  
var src = {
  styles         : "src/styles/",
  scripts        : "src/scripts/",
  templates      : "src/templates/",
  locales        : "src/locales/"
}

var langs = {
  fr: 'fr',
  en: 'en',
  pt: 'pt'
}

gulp.task( "browser-sync", function() {

  browserSync.init( [ "app/scripts/*.js", "app/css/*.css", "app/*.html", "app/assets/*" ], {
    server: {
      baseDir: "./app"
    },
    open: false
  } );
} );

gulp.task('styles', function () {
  return  gulp.src(src.styles + '*.scss')
        .pipe(compass({
          css: './app/css',
          // OLD COMPASS VERSION
          sass: './src/styles',
          // NEW COMPASS VERSION FIX
          // sass: 'src/styles',
          image: './app/assets'
        }))
        .pipe(gulp.dest('./app/css/'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest('./app/css'));
});

gulp.task( "jade-template", function() {
  return gulp.src( src.templates + "index.jade")
      .pipe( jade( { 
            pretty: true,
            basedir: "src/templates/",
            data: langs
          })
        )
        .on( "error", gutil.log )
        .on( "error", gutil.beep )
      .pipe( gulp.dest( "app/" ) );

} );

// gulp.task("i18n", function(){

//   return gulp.src('app/index.html')
//     .pipe(i18n({
//       langDir: './src/locales',
//       trace: true
//       })
//     )
//     .pipe(gulp.dest('./app/'))



// });

gulp.task("rename-fr", function(){

  return gulp.src('./app/index-fr.html')
    .pipe( rename( "index.html" ) )
    .pipe(gulp.dest('./app/'))

});



gulp.task('build-script', function() {

  gulp.src(src.scripts + "**/*.js")
    .pipe(concat('_vendor.js'))
    .pipe(gulp.dest('./tmp/'))

  return gulp.src( src.scripts + "main.coffee", { read: false } )
      .pipe( browserify({ 
        paths: [ "src/scripts/", "src/templates/modules" ], 
        transform: [ bpj, html, "coffeeify" ], 
        extensions: [ ".coffee" ]
        })
      )
      .on( "error", gutil.log )
      .on( "error", gutil.beep )
    .pipe( rename( "main.js" ) )
    .pipe(gulp.dest('./tmp/'));

});

gulp.task('clean-scripts', function () {

  gulp.src("./tmp/*.js")
    .pipe(concat('main.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('./app/scripts'));

  return gulp.src("./tmp/*.js").pipe(clean());
    
});

gulp.task('scripts', function() {
  runSequence('build-script', 'clean-scripts');
});

gulp.task('templates', function() {
  // runSequence('jade-template', 'i18n', 'rename-fr');
  runSequence('jade-template');
});

gulp.task( "watch", function() {

  gulp.watch( "src/styles/**/*.scss", [ "styles" ] );
  gulp.watch( "src/locales/**/*.js", [ "templates" ] );
  gulp.watch( "json/*.json", [ "scripts" ] );
  gulp.watch( "src/templates/**/*.jade", [ "templates" ] );
  gulp.watch( "src/scripts/**/*.coffee", [ "scripts"] );
  gulp.watch( "src/scripts/**/*.coffee", [ "scripts"] );


} );

gulp.task( "default", function(){
  runSequence( "browser-sync", "styles",  "scripts",  "templates" , "watch" )
});
