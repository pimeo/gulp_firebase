Person           = require 'Person'
ExercicesManager = require 'exercices/ExercicesManager'

class Student extends Person
  constructor: (classRoomRef, firstname, lastname) ->
    super classRoomRef, firstname, lastname

    # firebase specific student child
    @studentSlug = "students/student#{@id}"
    @studentRef  = @classRoomRef.child @studentSlug
    @studentRef.child('online').onDisconnect().remove()

  render: ->
    c = document.querySelector ".students"
    s = document.createElement "div"
    s.className = "student-#{@id}"

    t = document.createElement "h4"
    t.innerHTML = "Eleve #{@id}"

    n = document.createElement "h6"
    n.innerHTML = "#{@firstname} #{@lastname}"

    s.appendChild t
    s.appendChild n
    c.appendChild s

  # connect
  connect: ->
    @studentRef.child("online").on 'value', (online) =>
      console.log @studentSlug, "listen online", online.val()
      @tryToConnect() if online.val() == null

  # tryToConnect
  tryToConnect: ->
    console.log "trying connect...", @studentSlug + "/online"
    
    # Use a transaction to make sure we don't conflict with other people trying to join.
    @studentRef.child("online").transaction (onlineVal) =>
      return if onlineVal == null then true else false
      # error callback
    , (error, comitted) =>
      if comitted
        @render()
        @bindEvents()
      else 
        console.log "not ready"

  bindEvents: ->
    # launch exercice
    @studentRef.child('launchExercice').on 'value', @launchExercice

  launchExercice: (exercice) =>
    if exercice.val() != null
      console.log "ready for exercice", exercice.val()
      @setExercice exercice.val()

  # EXERCICES

  setExercice: ( @exerciceID ) ->
    # set exercice
    @studentRef.child("exercices/exercice#{@exerciceID}").set anwser: "", needHelp: no

    exercice = ExercicesManager.set @exerciceID, @onSelectAnwser, @onNeedHelp
    exercice.render ".student-#{@id}"

  onSelectAnwser: (selectedAnwser) =>
    console.log "student choose", selectedAnwser
    @studentRef.child("exercices/exercice#{@exerciceID}/").update anwser: selectedAnwser

  onNeedHelp: (exerciceID, question) =>
    console.log "need help", exerciceID, question
    @studentRef.child("exercices/exercice#{exerciceID}/").update needHelp: yes


module.exports = Student