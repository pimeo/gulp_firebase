__ID_CLASS_ROOM = 0

Node     = require 'Node'
Student  = require 'Student'
Teacher  = require 'Teacher'

class ClassRoom extends Node
  constructor: (@classRoomRef, @name) ->
    @classRoomID = __ID_CLASS_ROOM++
    @students    = []
    console.log "[ClassRoom] #{@classRoomID}"

    # --- TEACHER
    t = new Teacher @classRoomRef, "Maurice", "Teacher"

    # --- STUDENT ONE
    s = new Student @classRoomRef, "Mattis", "Durand"
    s.connect()

    # --- STUDENT TWO
    s = new Student @classRoomRef, "Elodie", "Doveze"
    s.connect()

module.exports = ClassRoom