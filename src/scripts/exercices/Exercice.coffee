# ----
# EXERCICE
# ----

class Exercice
  constructor: (datas, @callbackSelectAnwser, @callbackNeedHelp) ->
    @questionId = datas.questionId
    @question   = datas.question
    @answers    = datas.answers
    @id = Math.floor(Math.random() * 1000000)

  render: (wrapper) ->
    w = document.querySelector wrapper

    # template
    c = document.createElement "div"
    c.className = "question-#{@questionId}-#{@id}"
    w.appendChild c

    tq = document.createElement "h5"
    tq.innerHTML = @question
    c.appendChild tq

    tas = document.createElement "div"
    tas.className = "answers"
    c.appendChild tas

    for i in [0...@answers.length]
      a  = @answers[i]
      ta = document.createElement "div"
      ta.className = "answer"

      input = document.createElement "input"
      input.id = "answer-#{i}"
      input.type = "radio"
      input.name = "anwsers"
      input.value = a.val

      label = document.createElement "label"
      label.appendChild input
      text = document.createTextNode a.val
      label.appendChild text
      
      ta.appendChild label
      tas.appendChild ta

    # event on click checkbox
    inputs = document.querySelectorAll ".question-#{@questionId}-#{@id} input"
    input.addEventListener "click", @selectAnswer, no for input in inputs 

    # button 
    btnHelp = document.createElement "button"
    btnHelp.innerHTML = "Aide !"
    btnHelp.style.display = "block"
    ta.appendChild btnHelp
    btnHelp.addEventListener "click", @needHelp, no

  selectAnswer: (e) =>
    @callbackSelectAnwser e.currentTarget.value

  needHelp: (e) =>
    @callbackNeedHelp @questionId, @question


module.exports = Exercice