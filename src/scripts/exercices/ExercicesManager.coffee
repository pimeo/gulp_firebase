__ID_EXERCICE = 0

# ----
# EXERCICES
# ----

exos = [
  questionId: 0 
  question: "Quelle couleur est un perroquet ?"
  answers: [
    { id: 0, val: "Rouge", correct: yes }
    { id: 1, val: "Jaune" }
    { id: 2, val: "Marron" }
  ]
,
  questionId: 1
  question: "Que font 10 + 3 ?"
  answers: [
    { id: 0, val: 7 }
    { id: 1, val: 8 }
    { id: 2, val: 13, correct: yes }
  ]
]

# ----
# EXERCICES MANAGER
# ----

class ExercicesManagerSingleton
  instance = null
  @get: ->
    instance ?= new ExercicesManager()

  class ExercicesManager
    constructor: ->
      console.log "[EXERCICE MANAGER]"

    set: (exerciceID, callbackSelectAnwser, callbackNeedHelp) ->
      new (require 'exercices/Exercice') exos[exerciceID], callbackSelectAnwser, callbackNeedHelp


module.exports = ExercicesManagerSingleton.get()

