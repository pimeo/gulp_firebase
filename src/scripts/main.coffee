# ----
# APP
# ----

class App 
  constructor: ->

    @fire = new (require 'firebase') "brilliant-fire-9803.firebaseio.com/"
   
    @classRoomCP = @fire.child "classroom"

    # clean before made some modifications
    @classRoomCP.remove()
    @classRoomCP.set name: "cp", students: null

    # new classroom
    classRoom = new (require 'ClassRoom') @classRoomCP, "cp"

app = null
document.addEventListener 'DOMContentLoaded', () =>
  app = new App()