Person  = require 'Person'

class Teacher extends Person
  constructor: (classRoomRef, firstname, lastname) ->
    super classRoomRef, firstname, lastname

    @info = document.querySelector ".teacher p"

    @studentsRef = @classRoomRef.child('students')
    @studentsRef.on "child_added", (student) =>
      @log student.key(), "Nouvel élève connecté"
      
      # bind events for current student
      @bindStudent student.key()

      # launch exercice
      @classRoomRef.child("students/#{student.key()}/").update launchExercice: 0
      @log student.key(), "Lancement question 0"

  bindStudent: (studentId) ->
    # when student need help
    @classRoomRef.child("students/#{studentId}/exercices").on "child_changed", @onExerciceChanged

  # events callback
  onExerciceChanged: (datas) =>
    exercice = datas.key() 
    student  = datas.ref().parent().parent().key()
    result   = datas.val()

    if result.needHelp == yes
      @warn "Eleve demande de l'aide sur l'exercice #{exercice}", student
    else
      @log "Eleve a choisit la réponse #{result.anwser} #{exercice}", student

  log: (student, msg) ->
    @info.innerHTML += "#{msg} : <b>#{student}</b><br/>"

  warn: (student, msg) ->
    @info.innerHTML += "<span style='color: red'>#{msg} : <b>#{student}</b></span><br/>"


module.exports = Teacher